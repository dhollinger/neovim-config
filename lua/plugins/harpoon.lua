return {
  "ThePrimeagen/harpoon",
  branch = "harpoon2",
  dependencies = {
    "nvim-lua/plenary.nvim",
  },
  config = function()
    local harpoon = require('harpoon')
    local wk = require('which-key')

    harpoon:setup()

    -- wk.register({
    --   k = {
    --     name = "Harpoon",
    --     e = { function() harpoon:list():append() end, "Append to List" },
    --     h = { function() harpoon.ui:toggle_quick_menu(harpoon:list()) end, "Toggle UI"},
    --   }
    -- })

    -- vim.keymap.set("n", "<C-e>", function() harpoon:list():select(1) end)
    -- vim.keymap.set("n", "<C-t>", function() harpoon:list():select(2) end)
    -- vim.keymap.set("n", "<C-n>", function() harpoon:list():select(3) end)
    -- vim.keymap.set("n", "<C-s>", function() harpoon:list():select(4) end)
  end
}
