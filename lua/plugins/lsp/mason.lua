return {
  "williamboman/mason.nvim",
  dependencies = {
    "williamboman/mason-lspconfig.nvim",
    "WhoIsSethDaniel/mason-tool-installer.nvim",
  },
  config = function()
    local mason = require("mason")
    local mason_lspconfig = require("mason-lspconfig")
    local mason_tool_installer = require("mason-tool-installer")

    mason.setup({
      ui = {
        icons = {
          package_installed = "✓",
          package_pending = "➜",
          package_uninstalled = "✗",
        },
      },
    })

    mason_lspconfig.setup({
      ensure_installed = {
        "bashls",
        "bufls",
        "diagnosticls",
        "docker_compose_language_service",
        "dockerls",
        "gopls",
        "html",
        "jsonls",
        "lemminx",
        "lua_ls",
        "powershell_es",
        "puppet",
        "pyright",
        -- "solargraph",
        "vimls",
        "yamlls",
      },
      automatic_installation = true,
    })

    mason_tool_installer.setup({
      ensure_installed = {
        "shellcheck", -- bash linter
        "shfmt", -- bash formatter
        "stylua", -- lua formatter
        "black", -- python formatter
        "debugpy", -- python debugger
        "ruff", -- python linter and formatter
        "mypy", -- python static typing
        "hadolint", -- Dockerfile linting
        "protolint", -- protocol buffers linting
        "haml-lint", -- HAML file linting
        "vint", -- vimscript linter
        "htmlbeautifier", -- html formatter
        "jsonlint", -- JSON linter
        "yamlfmt", -- YAML formatter
        "commitlint", -- Git Commit linter
        "gitlint", -- Git linter
        "editorconfig-checker", -- editorconfig linter
        "rubocop", -- ruby linter and formatter
      },
    })
  end,
}
