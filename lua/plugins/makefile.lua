return {
  "sopa0/telescope-makefile",
  dependencies = {
    "akinsho/toggleterm.nvim",
  },
  opts = {},
  config = function(_, opts)
    require("telescope").setup(opts)
    require("telescope").load_extension "make"
  end,
}
