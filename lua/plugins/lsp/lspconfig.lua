return {
  "neovim/nvim-lspconfig",
  event = { "BufReadPre", "BufNewFile" },
  dependencies = {
    "hrsh7th/cmp-nvim-lsp",
    "nvimdev/lspsaga.nvim",
    { "antosha417/nvim-lsp-file-operations", config = true },
  },
  config = function()
    local lspconfig = require "lspconfig"
    local util = require "lspconfig/util"

    local cmp_nvim_lsp = require "cmp_nvim_lsp"

    local keymap = vim.keymap

    local opts = { noremap = true, silent = true }
    local on_attach = function(client, bufnr)
      opts.buffer = bufnr

      opts.desc = "Show LSP references"
      keymap.set("n", "gR", "<cmd>Telescope lsp_references<CR>", opts)

      opts.desc = "Go to declaration"
      keymap.set("n", "gD", vim.lsp.buf.declaration, opts)

      opts.desc = "Show LSP Definitions"
      keymap.set("n", "gd", "<cmd>Telescope lsp_definitions<CR>", opts)

      opts.desc = "Peek LSP Definitions"
      keymap.set("n", "<leader>gd", "<cmd>Lspsaga peek_definition<CR>", opts)

      opts.desc = "Show LSP implementations"
      keymap.set("n", "gi", "<cmd>Telescope lsp_implementations<CR>", opts)

      opts.desc = "Show LSP type definitions"
      keymap.set("n", "gt", "<cmd>Telescope lsp_type_definitions<CR>", opts)

      opts.desc = "Peek LSP type definitions"
      keymap.set("n", "<leader>gt", "<cmd>Lspsaga peek_type_definition<CR>", opts)

      opts.desc = "See available code actions"
      keymap.set({ "n", "v" }, "<leader>ca", "<cmd>Lspsaga code_action<CR>", opts)

      opts.desc = "See the outline"
      keymap.set("n", "<leader>A", "<cmd>Lspsaga outline<CR>", opts)

      opts.desc = "Smart rename"
      -- keymap.set("n", "<leader>rn", vim.lsp.buf.rename, opts)
      keymap.set("n", "<leader>rn", "<cmd>Lspsaga rename<CR>", opts)

      opts.desc = "Show buffer diagnostics"
      keymap.set("n", "<leader>D", "<cmd>Telescope diagnostics bufnr=0<CR>", opts)

      opts.desc = "Show line diagnostics"
      keymap.set("n", "<leader>d", vim.diagnostic.open_float, opts)

      opts.desc = "Go to previous diagnostic"
      keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)

      opts.desc = "Go to next diagnostic"
      keymap.set("n", "]d", vim.diagnostic.goto_next, opts)

      opts.desc = "Show documentations for what is under cursor"
      keymap.set("n", "K", "<cmd>Lspsaga hover_doc<CR>", opts)

      opts.desc = "Format using LSP"
      keymap.set("n", "<leader>fm", vim.lsp.buf.format, opts)

      opts.desc = "Restart LSP"
      keymap.set("n", "<leader>rs", "<cmd>LspRestart<CR>", opts)
    end

    local capabilities = cmp_nvim_lsp.default_capabilities()

    capabilities.textDocument.completion.completionItem = {
      documentationFormat = { "markdown", "plaintext" },
      snippetSupport = true,
      preselectSupport = true,
      insertReplaceSupport = true,
      labelDetailsSupport = true,
      deprecatedSupport = true,
      commitCharactersSupport = true,
      tagSupport = { valueSet = { 1 } },
      resolveSupport = {
        properties = {
          "documentation",
          "detail",
          "additionalTextEdits",
        },
      },
    }

    local signs = { Error = " ", Warn = " ", Hint = "󰠠 ", Info = " " }
    for type, icon in pairs(signs) do
      local hl = "DiagnosticSign" .. type
      vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
    end

    -- configure lua server (with special settings)
    lspconfig["lua_ls"].setup {
      capabilities = capabilities,
      on_attach = on_attach,
      before_init = require("neodev.lsp").before_init,
      settings = { -- custom settings for lua
        Lua = {
          completion = {
            callSnippet = "Replace",
          },
          -- make the language server recognize "vim" global
          diagnostics = {
            globals = { "vim" },
          },
          workspace = {
            checkThirdParty = false,
            -- make language server aware of runtime files
          },
        },
      },
    }

    -- configure gopls
    lspconfig["gopls"].setup {
      on_attach = on_attach,
      capabilities = capabilities,
      filetypes = {
        "go",
        "gomod",
        "gowork",
        "gotmpl",
      },
      root_dir = util.root_pattern("go.work", "go.mod", ".git"),
      settings = {
        gopls = {
          completeUnimported = true,
          usePlaceholders = true,
          analyses = {
            nilness = true,
            shadow = true,
            unusedparams = true,
            unusedwrites = true,
          },
          hints = {
            assignment_string = true,
            compositeLiteralFields = true,
            constantValues = true,
            functionTypeParameters = true,
            parameterNames = true,
            rangeVariableTypes = true,
          },
        },
      },
    }

    -- configure yamlls
    lspconfig["yamlls"].setup {
      on_attach = on_attach,
      capabilities = capabilities,
      filetypes = {
        "yaml",
      },
      settings = {
        yaml = {
          schemas = {
            ["https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master/v1.25.9-standalone-strict/all.json"] = "/argo/**/*.yaml",
            ["https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json"] = ".gitlab-ci.yml",
            ["https://json.schemastore.org/github-workflow.json"] = "/.github/workflows/*",
            ["https://json.schemastore.org/chart.json"] = "*Chart.yaml",
            ["https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master/v1.25.9/service-v1.json"] = "*service.yaml",
            ["https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master/v1.25.9/deployment-apps-v1.json"] = "*deployment.yaml",
            ["http://json.schemastore.org/kustomization"] = "kustomization.yaml",
            ["https://json.schemastore.org/cloudbuild.json"] = "cloudbuild*.yaml",
            ["https://dlvhdr.github.io/gh-dash/configuration/gh-dash/schema.json"] = "*/gh-dash/config*.yml",
          },
        },
      },
    }

    -- configure solargraph
    lspconfig["solargraph"].setup {
      on_attach = on_attach,
      capabilities = capabilities,
      filetypes = {
        "ruby",
      },
    }

    -- configure lemminx
    lspconfig["lemminx"].setup {
      on_attach = on_attach,
      capabilities = capabilities,
      filetypes = {
        "xml",
      },
    }

    lspconfig.puppet.setup {
      on_attach = on_attach,
      capabilities = capabilities,
      cmd = {
        "puppet-languageserver",
        "--stdio",
        "--puppet-settings=--modulepath,/home/dhollinger/.puppetlabs/etc/code/modules",
      },
      filetypes = { "puppet" },
      root_dir = util.root_pattern("Puppetfile", "metadata.json", ".git"),
    }
  end,
}
