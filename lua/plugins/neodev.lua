return {
  "folke/neodev.nvim",
  lazy = false,
  opts = {},
  config = function()
    require("neodev").setup {
      override = function(_, library)
        library.enabled = true
        library.plugins = true
      end,
      pathStrict = true,
      -- lspconfig  = true
    }
  end,
}
