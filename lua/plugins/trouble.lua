return {
  "folke/trouble.nvim",
  dependencies = {
    "nvim-tree/nvim-web-devicons",
  },
  event = "InsertEnter",
  config = function ()
    require("trouble").setup()
  end,
}
